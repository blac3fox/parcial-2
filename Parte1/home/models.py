from django.db import models

# Create your models here.

class Classificacion(models.Model):
    nombre = models.CharField(max_length=255)
    identi = models.CharField(max_length=255)
    slug = models.SlugField()

    def __str__(self):
        return self.nombre

class TrabajosInv(models.Model):
    Titulo = models.CharField(max_length=255)
    Tipo = models.ForeignKey(Classificacion, on_delete=models.CASCADE)
    Autor = models.CharField(max_length=255)
    Fecha = models.DateField()
    slug = models.SlugField()

    def __str__(self):
        return  self.Titulo
