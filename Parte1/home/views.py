from django.db.models import Q
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.views import generic

from .forms import Users_Form , LoginForm
from django.urls import reverse_lazy
from django.contrib.auth import authenticate , login

from .models import TrabajosInv
from .forms import TrabajoForm

#LOGIN
def index(request):
	message = "Not Login"
	form = LoginForm(request.POST or None)
	if request.method == "POST":
		form = LoginForm(request.POST or None)
		if form.is_valid():
			username = request.POST["username"]
			password = request.POST["password"]
			user = authenticate(username=username, password=password)
			if user is not None:
				if user.is_active:
					login(request , user)
					message = "User Logged"
				else:
					message = "User is Not Active"
			else:
				message = "Username or Password is not Correct"
		context = {
			"form": form
		}
	context = {
		"form": form,
	}
	return render(request, "home/index.html", context)

class  Signup(generic.FormView):
	template_name = 'home/signup.html'
	form_class = Users_Form
	success_url = reverse_lazy('login')

	def form_valid(self , form):
		user = form.save()
		return super(Signup, self).form_valid(form)

# CREATE

class create(generic.CreateView):
	template_name = "home/create.html"
	model = TrabajosInv
	fields = [
		"Titulo",
		"Tipo",
		"Autor",
		"Fecha",
		"slug",
	]
	success_url = reverse_lazy("list")

# RETRIEVE

class List(generic.ListView):
	template_name = "home/list.html"
	def get_queryset(self, *args, **kwargs):
		qs = TrabajosInv.objects.all()
		print(self.request)
		query = self.request.GET.get("q", None)
		print(query)
		if query is not None:
			qs = qs.filter(Q(Autor__icontains=query))
		return qs

#DETAIL

class Detail(generic.DetailView):
	template_name = "home/detail.html"
	model = TrabajosInv

#UPDATE

class Update(generic.UpdateView):
	template_name = "home/update.html"
	model = TrabajosInv
	fields = [
		"Titulo",
		"Tipo",
		"Autor",
		"Fecha",
		"slug",
	]
	success_url = reverse_lazy("list")
