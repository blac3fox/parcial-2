from django.urls import path

from django.contrib.auth.views import logout_then_login

from home import views

urlpatterns = [
    path('', views.index, name="login"),
    path('list/', views.List.as_view(), name="list"),
    path('detail/<int:pk>/', views.Detail.as_view(), name="detail"),
    path('update/<int:pk>/', views.Update.as_view(), name="update"),
    path('create/', views.create.as_view(), name="create"),
    path('cerrar/' , logout_then_login  , name = 'logout' ),
	path('signup/', views.Signup.as_view(), name="signup"),

]