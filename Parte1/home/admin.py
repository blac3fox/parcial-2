from django.contrib import admin
from .models import TrabajosInv, Classificacion

# Register your models here.
@admin.register(TrabajosInv)
class AdminTrabajo(admin.ModelAdmin):
    list_display = [
        "Titulo",
        "Tipo",
        "Autor",
        "Fecha",
    ]

@admin.register(Classificacion)
class AdminClass(admin.ModelAdmin):
    list_display = [
        "nombre",
    ]
