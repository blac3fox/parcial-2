from django import forms
from .models import TrabajosInv
from django.contrib.auth.forms import UserCreationForm

class LoginForm(forms.Form):
    username = forms.CharField(max_length=24, widget=forms.TextInput())
    password = forms.CharField(max_length=24, widget=forms.PasswordInput())

class Users_Form(UserCreationForm):
    email = forms.CharField(max_length=48)

class TrabajoForm(forms.ModelForm):
    trabajo_titulo = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "Titulo del trabajo", "class": "form-control"}))
    trabajo_autor = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "Autor del trabajo", "class": "form-control"}))
    trabajo_fecha = forms.DateField(
        widget=forms.DateField())
    slug = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "Slug", "class": "form-control"}))
    class Meta:
        model = TrabajosInv
        fields=[
            "Titulo",
            "Tipo",
            "Autor",
            "Fecha",
            "slug",
        ]

