from django.contrib import admin

# Register your models here.
from .models import Departamento, Software

@admin.register(Software)
class SoftwareAdmin(admin.ModelAdmin):
	list_display = [
		"nombre_soft",
		"funcion",
		"departamento",
		"slug",
		]

@admin.register(Departamento)
class DepartamentoAdmin(admin.ModelAdmin):
	list_display = [
		"nombre_dep",
		"slug",
		]