from django.shortcuts import render, redirect

from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

#Modelos y formas
from .models import Departamento, Software
from .forms import DepartamentoForm, SoftwareForm

class List(generic.ListView):
	template_name = "home/list.html"
	queryset = Software.objects.filter()

	def get_queryset(self, *args, **kwargs):
		qs = Software.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(Q(nombre_soft__icontains=query))
		return qs

class Create(generic.CreateView):
	template_name = "home/create.html"
	model = Software
	fields = [
		"nombre_soft",
		"funcion",
		"departamento",
		"slug",
	]
	success_url = reverse_lazy("list")

class CreateDep(generic.CreateView):
	template_name = "home/createdep.html"
	model = Departamento
	fields = [
		"nombre_dep",
		"slug",
	]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "home/update.html"
	model = Software
	fields = [
		"nombre_soft",
		"funcion",
		"departamento",
		"slug",
	]
	success_url = reverse_lazy("list")

class UpdateDep(generic.UpdateView):
	template_name = "home/updatedep.html"
	model = Departamento
	fields = [
		"nombre_dep",
		"slug",
	]
	success_url = reverse_lazy("list")