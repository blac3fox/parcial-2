from django import forms

from .models import Departamento, Software

class DepartamentoForm(forms.ModelForm):
	class Meta:
		model = Departamento
		fields = [
		"nombre_dep",
		"slug",
		]

class SoftwareForm(forms.ModelForm):
	class Meta:
		model = Software
		fields = [
		"nombre_soft",
		"funcion",
		"departamento",
		"slug",
		]