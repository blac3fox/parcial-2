from django.urls import path, include
from home import views

urlpatterns = [
	path('', views.List.as_view(), name = "list"),
	path('create/', views.Create.as_view(), name = "create"),
	path('createdep/', views.CreateDep.as_view(), name = "createdep"),
	path('update/<int:pk>/', views.Update.as_view(), name = "update"),
	path('updatedep/<int:pk>/', views.UpdateDep.as_view(), name = "updatedep"),
]
